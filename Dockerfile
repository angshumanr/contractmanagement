FROM redhat/ubi8

RUN yum -y update
RUN yum -y remove java
RUN yum install -y \
       java-1.8.0-openjdk \
       java-1.8.0-openjdk-devel
CMD ["java", "-version"]
WORKDIR /
#RUN mkdir -p /opt/wsil/eie/certs
#ADD src/KEYSTORE_TMO_CERT /opt/wsil/eie/certs/KEYSTORE_TMO_CERT
ADD src/ContractManagementREST-V1.0.jar ContractManagement-1.0.jar
EXPOSE 10097
ENTRYPOINT exec java -jar ContractManagement-1.0.jar